package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     * random * ( upper - lower +1 ) + lower
     */
    public int ranNum(int lowNum, int upNum) {

        int ranNum = (int) (Math.random( )* (upNum - lowNum) + 1);

        return ranNum;

    }








    private void start() {




        System.out.println("Lower bound?" + "");
        int lowNum = Integer.parseInt( Keyboard.readInput(  ) );

        System.out.println("Upper bound?" + "");
        int upNum = Integer.parseInt( Keyboard.readInput( ) );

        int ranNum1 = ranNum( lowNum, upNum);
        int ranNum2 = ranNum( lowNum, upNum);
        int ranNum3 = ranNum( lowNum, upNum);

        System.out.println("3 randomly generated number: " + ranNum1 + ", " + ranNum2 + " and " + ranNum3);

        int smalla = Math.min (ranNum1,ranNum2);
        int smallb = Math.min (smalla, ranNum3);

        System.out.println("Smallest number is " + smallb);






    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
