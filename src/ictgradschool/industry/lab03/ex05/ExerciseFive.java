package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {

        String getRow = letters.substring((row - 1) * 6, (row - 1) * 6 + 6);


        return getRow;


    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {

        String letters = " ";

        row1 = getRow(letters, 1);
        row2 = getRow(letters, 2);
        row3 = getRow(letters, 3);

        System.out.println(row1);
        System.out.println(row2);
        System.out.println(row3);


    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {

        String leftDiagonal = row1.valueOf(row1.charAt(0)) + " " + row2.valueOf(row2.charAt(2)) + " " + row3.valueOf(row3.charAt(4));

        return leftDiagonal;

    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {

        System.out.println(leftDiagonal);


    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        ex.start();
    }
}
